const firstArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const secondArr = ["1", "2", "3", "sea", "user", 23];
const thirdArr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

function arrayToList (input) {
    return input.map(elem => {
        if(Array.isArray(elem)) return `<ul>${arrayToList(elem)}</ul>`;
        return `<li>${elem}</li>`;
    }).join('');
}

function createListInHtml (input, parent = document.body) {
    parent.insertAdjacentHTML('beforeend', `<ul>${arrayToList(input)}</ul>`);
}

createListInHtml(firstArr);
createListInHtml(secondArr);
createListInHtml(thirdArr);
